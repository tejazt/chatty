<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => '/api/v1/'], function(){

/*
	User Routes
 */
Route::post('user/signup', 'UserController@signup');
Route::post('user/signin', 'UserController@signin');
Route::get('user/show', 'UserController@getUserInfo');
Route::get('user/chatrooms', 'UserController@getUserChatRooms');
Route::post('user/chatrooms/{id}/join', 'UserController@joinChatRoom');
Route::post('user/chatrooms/{id}/leave', 'UserController@leaveChatRoom');

/*
	Message Routes
 */
Route::post('message/create', 'MessageController@create');

/*
	ChatRoom Routes
 */
Route::post('chatroom/create', 'ChatRoomController@create');
Route::get('chatroom/{id}', 'ChatRoomController@getChatRoom');
Route::get('chatroom/search/{name}', 'ChatRoomController@searchChatRoom');

});