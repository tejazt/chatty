<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['user_id', 'chat_room_id', 'message'];
	
	public function user(){
		return $this->belongsTo('App\User');
	}

	public function chat_room(){
		return $this->belongsTo('App\ChatRoom');
	}
}
