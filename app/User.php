<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
// use Mpociot\Firebase\SyncsWithFirebase;

class User extends Authenticatable
{
    // use SyncsWithFirebase;

    protected $fillable = ['name', 'email', 'username', 'password', 'token'];

    protected $hidden = ['password', 'remember_token',];

    public function messages(){
        return $this->hasMany('App\Message');
    }

    public function chat_rooms(){
        return $this->belongsToMany('App\ChatRoom', 'chat_room_user');
    }
}
