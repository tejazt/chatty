<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\ChatRoom;
use App\Message;
use JWTAuth;

class ChatRoomController extends Controller
{
	public function __construct(){
        $this->middleware('jwt.auth');
    }

    public function create(Request $request){
    	$this->validate($request, [
    		'name' => 'required'
    	]);

    	if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json(['message' => 'Cannot find user'], 404);
        }

        $chatRoom = new ChatRoom([
        	'name' => $request['name']
        ]);

        if(!$chatRoom->save())
        	return response()->json(['message' => 'Error creating new chat'], 404);

        $user->chat_rooms()->attach($chatRoom->id);

      	$response = [
      		'message' => 'New chat created',
      		'chat_room' => $chatRoom
      	];
        return response()->json($response, 200);
    }

    public function getChatRoom($id){
    	if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json(['message' => 'Cannot find user'], 404);
        }

        $chatRoom = ChatRoom::findOrFail($id);

        if(!$chatRoom)
        	return response()->json(['message' => 'Chat room not found'], 404);

        $users = $chatRoom->users;

        $messages = $chatRoom->messages;

        foreach($messages as $message){
        	$message->view_user = $message->user()->first();
        }

        $response = [
        	'message' => 'Chat room info: '.$chatRoom->name,
        	'chat_room' => $chatRoom,
        ];

        return response()->json($response, 200);
    }

    public function searchChatRoom($name){
    	
    	if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json(['message' => 'Cannot find user'], 404);
        }

        $user_chat_rooms = $user->chat_rooms->pluck('id')->toArray();

    	$chat_rooms = ChatRoom::where('name', 'like', '%'.$name.'%')->whereNotIn('id', $user_chat_rooms)->get();
    	if(!$chat_rooms)
    		return response()->json(['message' => 'No chat rooms found'], 200);

    	$response = [
    		'message' => 'Showing search results for '.$name,
    		'chat_rooms' => $chat_rooms
    	];

    	return response()->json($response, 200);
    }
}
