<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\ChatRoom;
use App\Message;
use JWTAuth;

class MessageController extends Controller
{
	public function __construct(){
		return $this->middleware('jwt.auth');
	}

	public function create(Request $request){		

		$this->validate( $request, [
			'chat_room_id' => 'required|numeric',
			'message' => 'required'
			]);

		if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json(['message' => 'Cannot find user'], 404);
        }

		$chat_room_id = $request['chat_room_id'];
		$msg = $request['message'];

		$message = new Message([
			'chat_room_id' => $chat_room_id,
			'message' => $msg,
		]);

		if(!$user->messages()->save($message))
			return response()->json(['message' => 'Error creating message'], 404);

		$response = [
			'message' => 'Message Created',
			'message_details' => $message,
		];

		return response()->json($response, 200);
    }
}
