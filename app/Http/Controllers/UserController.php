<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\ChatRoom;
use App\Message;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;

class UserController extends Controller
{

    public function __construct(){
        $this->middleware('jwt.auth', ['except' => ['signup', 'signin']]);
    }

    public function signup(Request $request){

        $this->validate($request, [
            'name' => 'required|min:4|max:20',
            'username' => 'required|min:4|max:20',
            'email' => 'required|email',
            'password' => 'required',
            //'token' => 'required'
        ]);
         
        $name = $request->input('name');
        $username = $request->input('username');
        $email = $request->input('email');
        $password = bcrypt($request->input('password'));

        $user = new User([
            'name' => $name,
            'username' => $username,
            'email' => $email,
            'password' => $password
        ]);

        if(!$user->save()){
            $response = [
                'message' => 'Error occured while creating User',
            ];
            return response()->json($response, 404);
        }

        $user->signin = [
            'href' => 'api/v1/user/signin',
            'method' => 'POST',
            'params' => 'email,password'
        ];

        $response = [
            'message' => 'User Created',
            'user' => $user
        ];

        return response()->json($response, 201);
    }

    public function signin(Request $request){
        $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $credentials = $request->only('email','password');
        try{
            if(! $token = JWTAuth::attempt($credentials)){
                return response()->json(['message' => 'Invalid Credentials!'], 401);
            }
        }catch(JWTException $e){
            return response()->json(['message' => 'Could not generate token'], 500);
        }
        $user = User::where('email', $request->email)->first();
        return response()->json(['token' => $token, 'user' => $user]);
        
    }

    public function getUserInfo(){
        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json(['message' => 'Cannot find user'], 404);
        }
        return response()->json($user, 200);
    }


    public function getUserChatRooms(){
        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json(['message' => 'Cannot find user'], 404);
        }
        $chats = $user->chat_rooms;

        if(!$chats)
            return response()->json(['message' => 'You have no chats currently'], 200);

        $response = [
            'message' => 'List of all chats by user '.$user->username,
            'chats' => $chats
        ];

        return response()->json($response, 200);
    }

    public function joinChatRoom($id){
        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json(['message' => 'Cannot find user'], 404);
        }

        $chat_room = ChatRoom::findOrFail($id);
        if(!$chat_room)
            return response()->json(['message' => 'Could not find chat room'], 404);

        $user->chat_rooms()->attach($id);

        return response()->json(['message' => ' You have joined the chat '.$chat_room->name], 200);
    }

    public function leaveChatRoom($id){
        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json(['message' => 'Cannot find user'], 404);
        }

        $chat_room = ChatRoom::findOrFail($id);
        if(!$chat_room)
            return response()->json(['message' => 'Could not find chat room'], 404);

        $user->chat_rooms()->detach($id);

        return response()->json(['message' => ' You have left the chat '.$chat_room->name], 200);
    }












/*
    public function notify(){

        $tokens = User::pluck('token');
        $message = "Chatty Test Notification!";

        $url = 'https://fcm.googleapis.com/fcm/send';
 
        $fields = array(
                'registration_ids' => $tokens,
                'date' => $message
            );

        $headers = array(
            'Authorization: key=' 
            . 'AAAA2xaB05U:APA91bEqoeYDKZDjnkUbKyLzHO4a3mb3LAbLhDjUL7yvOTrvAA8enz_4b-GFx7c7m_GtEIQYDvv2U37CZ_UktkSVXwMJUqAGSCFgUaMqmj5Pf0VUSXL-olUsKFIgUx_t-QzqrSEZawrK6-uEpYRZBM4jTrXlw0O8rg',
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();
 
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
 
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
 
        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
 
        // Close connection
        curl_close($ch);
 
        return $result;
    }
*/
}
